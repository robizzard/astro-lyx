
## Write astronomy articles with LyX

Do you want to write astronomy or astrophysics articles quickly, without all the faff of learning every aspect of LaTeX, in a professional editor that includes spellchecking, version control, a referencing search interface, graphics - everything you could want from a word processor but perfect for MNRAS, A&A etc.? Then LyX is for you.

This repository contains files and help that will enable you to use LyX (from www.lyx.org) to write texts in astronomy and astrophysics.

Please see the instructions in astro-lyx.md (generated from astro-lyx.lyx using make_markdown.sh, there is also associated PDF version astro-lyx.pdf).
