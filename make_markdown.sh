#!/bin/bash


#
# make markdown version of astro-lyx.lyx
#

# use lyx to export astro-lyx.lyx in latex
lyx --export latex astro-lyx.lyx
# remove inputenc and fontenc (these freeze pandoc)
cat astro-lyx.tex |grep -v inputenc|grep -v fontenc > a.tex ; mv a.tex astro-lyx.tex
# run through pandoc to make markdown version
iconv -t utf-8 astro-lyx.tex | pandoc -s --wrap=none -f latex -t markdown | perl -pe "s/\{.*? \.unnumbered\}//g; s/\\\$\\\\rightarrow\\\$/\&rarr;/g;" > astro-lyx.md

# export also as pdf
lyx --export pdf astro-lyx.lyx
